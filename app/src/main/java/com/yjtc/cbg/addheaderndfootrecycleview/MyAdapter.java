package com.yjtc.cbg.addheaderndfootrecycleview;

import android.content.Context;

import java.util.List;

/**
 * Created by chenboge on 16/5/12.
 */
public class MyAdapter extends BaseAdapter<String,BaseViewHolder> {

    public MyAdapter(Context mContext, List<String> mDatas, int itemViewID) {
        super(mContext, mDatas, itemViewID);
    }

    @Override
    void bindData(BaseViewHolder holder, int position) {
        String s = mDatas.get(position);
        holder.getTextView(R.id.id_content).setText(mDatas.get(position));
    }
}
