package com.yjtc.cbg.addheaderndfootrecycleview;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;


import com.bartoszlipinski.recyclerviewheader.RecyclerViewHeader;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerViewHeader header;
    private RecyclerView recyclerView;
    private MyAdapter mAdapter;
    private MyNextAdapter mAdapter1;
    private List<String> mDatas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initDaya();
        initView();
    }

    private void initDaya() {
        mDatas = new ArrayList<>();
        for(int i=0;i<120;i++) {
            mDatas.add("成都市新都区西南石油大学");
        }
    }

    private void initView() {
        header = (RecyclerViewHeader) findViewById(R.id.header);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        mAdapter = new MyAdapter(this, mDatas, R.layout.template_text);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        header.attachTo(recyclerView, true);
    }


}
